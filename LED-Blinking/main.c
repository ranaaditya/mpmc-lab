#include <stdlib.h>
#include "hw_types.h"
#include "prcm.h"
#include "gpio.h"
#include "rom.h"
#include "utils.h"
#include "hw_apps_rcm.h"
#include "hw_memmap.h"


void main()
{
    PRCMPeripheralClkEnable(PRCM_GPIOA1, PRCM_RUN_MODE_CLK); //Setting the Clock for the port
    GPIODirModeSet(GPIOA1_BASE, 0XFF,GPIO_DIR_MODE_OUT);     //Enabling the pin

    //PinMuxConfig();
    while(1)
          {
            GPIOPinWrite(GPIOA1_BASE, 0XFF, 0x02);         //Writing pin 2 as high
            UtilsDelay(80000000/3);                        //Delay for 1 second
            GPIOPinWrite(GPIOA1_BASE, 0XFF, 0x00);         //Writing pin 2 as low
            UtilsDelay(80000000/3);                        //Delay for 1 second

            //HERE
            //0x02 - 0000 0010 - in pinmux tool we had put this as pin 2 HIGH
            //0x00 - 0000 0000 - in pinmux tool we had put this as pin 2 LOW
          }
}
/*
   Code for Multi Led blinking (oct 6 class)

   GPIOPinWrite(GPIOA1_BASE, 0XFF, 0x02);   0000 0010 - pin 2 LED glows
   UtilsDelay(80000000);
   GPIOPinWrite(GPIOA1_BASE, 0XFF, 0x04);   0000 0010 - pin 3 LED glows
   UtilsDelay(80000000);
   GPIOPinWrite(GPIOA1_BASE, 0XFF, 0x06);   0000 0110 - pin 2 and pin 3 LED glow together
   UtilsDelay(80000000);
 */
